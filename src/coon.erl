%%% @doc
%%% Currently Online Observation Network
%%% @end

-module(coon).
-vsn("0.1.0").
-behavior(application).
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").

-export([listen/1, ignore/0]).
-export([start/2, stop/1]).

-export_type([id/0, address/0, public_key/0]).


%%% Types

-type id() :: binary().
-type address() :: binary().
-type public_key() :: binary().



%%% Interface

-spec listen(PortNum) -> Result
    when PortNum :: inet:port_num(),
         Result  :: ok
                  | {error, {listening, inet:port_num()}}.
%% @doc
%% Make the server start listening on a port.
%% Returns an {error, Reason} tuple if it is already listening.

listen(PortNum) ->
    coon_client_man:listen(PortNum).


-spec ignore() -> ok.
%% @doc
%% Make the server stop listening if it is, or continue to do nothing if it isn't.

ignore() ->
    coon_client_man:ignore().


-spec start(normal, term()) -> {ok, pid()}.
%% @private
%% Hardcoding 6174 as the port

start(normal, _) ->
    Result = coon_sup:start_link(),
    ok = coon_client_man:listen(6174),
    Result.


-spec stop(term()) -> ok.

stop(_State) ->
    ok.
