%%% @doc
%%% Currently Online Observation Network Client
%%% @end

-module(coon_client).
-vsn("0.1.0").
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").

%% Peer interface
-export([connect/2]).
%% System interface
-export([start/1]).
-export([start_link/1, init/2]).
-export([system_continue/3, system_terminate/4,
         system_get_state/1, system_replace_state/2]).

-include("$zx_include/zx_logger.hrl").


%%% Type and Record Definitions


-record(s,
        {socket  = none            :: none | gen_tcp:socket(),
         pk      = none            :: none | public_key:rsa_public_key(),
         pk_der  = none            :: none | binary(),
         id      = <<0:512>>       :: binary(),
         wan     = none            :: none | {inet:address(), inet:port_number()},
         wan_bin = <<0:128, 0:16>> :: binary(),
         lan     = none            :: none | {inet:address(), inet:port_number()},
         lan_bin = <<0:128, 0:16>> :: binary(),
         blob    = none            :: none | binary()}).


-type state() :: #s{}.



%%% Peer interface

-spec connect(Peer, Address) -> Result
    when Peer    :: pid(),
         Address :: coon:address(),
         Result  :: ok
                  | {error, noproc | timeout | term()}.
%% @doc
%% Instruct the Peer to connect to the given Address.

connect(Peer, Address) ->
    call(Peer, {connect, Address}).


%% Helper 

call(PID, Message) ->
    Mon = monitor(process, PID),
    PID ! {Mon, Message},
    receive
        {reply, Mon, Result} ->
            true = demonitor(Mon),
            Result;
        {'DOWN', Mon, process, PID, noproc} ->
            {error, noproc};
        {'DOWN', Mon, process, PID, Reason} ->
            ok = log(info, "~w went down with ~w", [PID, Reason]),
            {error, noproc}
        after 5000 ->
            true = demonitor(Mon),
            {error, timeout}
    end.


%%% System Interface


start(ListenSocket) ->
    coon_client_sup:start_acceptor(ListenSocket).


start_link(ListenSocket) ->
    proc_lib:start_link(?MODULE, init, [self(), ListenSocket]).


-spec init(Parent, ListenSocket) -> no_return()
    when Parent       :: pid(),
         ListenSocket :: gen_tcp:socket().

init(Parent, ListenSocket) ->
    Debug = sys:debug_options([]),
    ok = proc_lib:init_ack(Parent, {ok, self()}),
    listen(Parent, Debug, ListenSocket).


listen(Parent, Debug, ListenSocket) ->
    case gen_tcp:accept(ListenSocket) of
        {ok, Socket} ->
            {ok, _} = start(ListenSocket),
            {ok, WAN} = zx_net:peername(Socket),
            WANstring = zx_net:host_string(WAN),
            ok = tell(info, "Connection accepted from: ~s", [WANstring]),
            State = #s{socket = Socket, wan = WAN},
            wait(Parent, Debug, State);
        {error, closed} ->
            ok = log(info, "Retiring: Listen socket closed."),
            exit(normal)
     end.

wait(Parent, Debug, State = #s{socket = Socket}) ->
    ok = inet:setopts(Socket, [{active, once}]),
    receive
        {tcp, Socket, <<"COON NODE 1">>} ->
            Blob = blob(),
            ok = gen_tcp:send(Socket, Blob),
            greet(Parent, Debug, State#s{blob = Blob});
        {tcp, Socket, _} ->
            retire(Socket, "Retiring. Received garbage");
        {tcp_closed, Socket} ->
            log(info, "Retiring. Socket closed before greet/3.")
        after 5000 ->
            timeout(Socket)
    end.

greet(Parent, Debug, State = #s{socket = Socket}) ->
    ok = inet:setopts(Socket, [{active, once}]),
    receive
        {tcp, Socket, <<Signature:64/binary, Signed/binary>>} ->
            verify(Parent, Debug, State, Signature, Signed);
        {tcp, Socket, _} ->
            retire(Socket, "Retiring. Received garbage");
        {tcp_closed, Socket} ->
            log(info, "Retiring. Socket closed before verify/5.")
        after 5000 ->
            timeout(Socket)
    end.

verify(Parent,
       Debug,
       State = #s{socket = Socket, blob = Blob, wan = WAN},
       Signature,
       Message = <<PKS:16, PK_DER:PKS/binary, NodeID:64/binary, LANbin:18/binary>>) ->
    PK = decode(PK_DER, Socket),
    Signed = <<Message/binary, Blob/binary>>,
    case vinz:verify(Signed, Signature, PK) of
        true ->
            LAN = bton(LANbin),
            WANbin = ntob(WAN),
            NewState = State#s{id      = NodeID,
                               pk      = PK,
                               pk_der  = PK_DER,
                               wan_bin = WANbin,
                               lan     = LAN,
                               lan_bin = LANbin},
            check_id(Parent, Debug, NewState);
        false ->
            retire(Socket, "Received garbage. Terminating.")
    end;
verify(_, _, #s{socket = Socket}, _, _) ->
    retire(Socket, "Received garbage. Terminating.").

decode(PK_DER, Socket) ->
    try
        public_key:der_decode('RSAPublicKey', PK_DER)
    catch
        _:_ -> retire(Socket, "Retiring. Bad DER format for public key.")
    end.

bton(<<A:16, B:16, C:16, D:16, E:16, F:16, G:16, H:16, Port:16>>) ->
    {{A, B, C, D, E, F, G, H}, Port}.

ntob({{A, B, C, D, E, F, G, H}, Port}) ->
    <<A:16, B:16, C:16, D:16, E:16, F:16, G:16, H:16, Port:16>>;
ntob({{A, B, C, D}, Port}) ->
    <<0:80, 16#FFFF:16, A:8, B:8, C:8, D:8, Port:16>>.

check_id(Parent, Debug, State = #s{id = ID, pk_der = PK_DER, socket = Socket}) ->
    Hash = crypto:hash(sha512, PK_DER),
    case Hash == ID of
        true  -> loop(Parent, Debug, State);
        false -> retire(Socket, "Invalid node ID.")
    end.


loop(Parent, Debug, State = #s{socket = Socket}) ->
    ok = inet:setopts(Socket, [{active, once}]),
    receive
        % INCOMING
        % Request random list of nodes
        % Lookup a node
        % Connect to a node

        % OUTGOING
        % UDP initiation instruction {Host, FromPort, ToPort}
        % Ping
        {tcp, Socket, Message} ->
            ok = log(info, "Recieved ~w", [Message]),
            loop(Parent, Debug, State);
        {tcp_closed, Socket} ->
            ok = log(info, "Socket closed, retiring."),
            exit(normal);
        {system, From, Request} ->
            sys:handle_system_msg(Request, From, Parent, ?MODULE, Debug, State);
        Unexpected ->
            ok = log(warning, "Unexpected message: ~tp", [Unexpected]),
            loop(Parent, Debug, State)
    end.


system_continue(Parent, Debug, State) ->
    loop(Parent, Debug, State).


system_terminate(Reason, _, _, _) ->
    exit(Reason).


system_get_state(State) -> {ok, State}.


system_replace_state(StateFun, State) ->
    {ok, StateFun(State), State}.



%%% Utility Functions

timeout(Socket) ->
    retire(Socket, "Timed out. Retiring.").


retire(Socket, Message) ->
    ok = log(info, Message),
    ok = gen_tcp:close(Socket),
    exit(normal).


blob() ->
    R = rand:uniform(340282366920938463463374607431768211455),
    <<R:128>>.
