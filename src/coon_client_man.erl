%%% @doc
%%% Currently Online Observation Network Client Manager
%%%
%%% This process maintains the registry that maps node IDs to network locations and
%%% public keys.
%%% @end

-module(coon_client_man).
-vsn("0.1.0").
-behavior(gen_server).
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").

% Service Interface
-export([listen/1, ignore/0]).
% Client Interface
-export([enroll/2, lookup/1]).
% gen_server
-export([start_link/0]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         code_change/3, terminate/2]).

-include("$zx_include/zx_logger.hrl").


%%% Type and Record Definitions


-record(s,
        {port_num = none :: none | inet:port_number(),
         listener = none :: none | gen_tcp:socket(),
         clients  = #{}  :: #{pid() => {ID :: binary(), Mon :: reference()}}}).

-record(node,
        {id   = <<>> :: binary(),
         pk   = <<>> :: binary(),
         pid  = none :: none | pid()}).


-type state() :: #s{}.



%%% Service Interface


-spec listen(PortNum) -> Result
    when PortNum :: inet:port_number(),
         Result  :: ok
                  | {error, Reason},
         Reason  :: {listening, inet:port_number()}.
%% @doc
%% Tell the service to start listening on a given port.
%% Only one port can be listened on at a time in the current implementation, so
%% an error is returned if the service is already listening.

listen(PortNum) ->
    gen_server:call(?MODULE, {listen, PortNum}).


-spec ignore() -> ok.
%% @doc
%% Tell the service to stop listening.
%% It is not an error to call this function when the service is not listening.

ignore() ->
    gen_server:cast(?MODULE, ignore).



%%% Client Interface


-spec enroll(ID, PublicKey) -> ok
    when ID        :: binary(),
         PublicKey :: binary().
%% @doc
%% Clients register here when they establish a connection.
%% Other processes can enroll as well.

enroll(ID, PublicKey) ->
    gen_server:cast(?MODULE, {enroll, ID, self(), PublicKey}).


-spec lookup(ID) -> Result
    when ID :: coon:id(),
         Result :: {ok, coon:public_key(), pid()}
                 | {ok, coon:public_key()}
                 | error.

lookup(ID) ->
    case ets:lookup(nodes, ID) of
        [#node{pid = none, pk = PK}] -> {ok, PK};
        [#node{pid = PID,  pk = PK}] -> {ok, PK, PID};
        []                           -> error
    end.



%%% Startup Functions


-spec start_link() -> Result
    when Result :: {ok, pid()}
                 | {error, Reason :: term()}.
%% @private
%% This should only ever be called by coon_clients (the service-level supervisor).

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, none, []).


-spec init(none) -> {ok, state()}.
%% @private
%% Called by the supervisor process to give the process a chance to perform any
%% preparatory work necessary for proper function.

init(none) ->
    TableOptions = [set, named_table, {read_concurrency, true}, {keypos, #node.id}],
    nodes = ets:new(nodes, TableOptions),
    State = #s{},
    {ok, State}.



%%% gen_server


handle_call({listen, PortNum}, _, State) ->
    {Response, NewState} = do_listen(PortNum, State),
    {reply, Response, NewState};
handle_call(Unexpected, From, State) ->
    ok = io:format("~p Unexpected call from ~tp: ~tp~n", [self(), From, Unexpected]),
    {noreply, State}.


handle_cast({enroll, ID, PID, PK}, State) ->
    NewState = do_enroll(ID, PID, PK, State),
    {noreply, NewState};
handle_cast(ignore, State) ->
    NewState = do_ignore(State),
    {noreply, NewState};
handle_cast(Unexpected, State) ->
    ok = io:format("~p Unexpected cast: ~tp~n", [self(), Unexpected]),
    {noreply, State}.


-spec handle_info(Message, State) -> {noreply, NewState}
    when Message  :: term(),
         State    :: state(),
         NewState :: state().
%% @private
%% The gen_server:handle_info/2 callback.
%% See: http://erlang.org/doc/man/gen_server.html#Module:handle_info-2

handle_info({'DOWN', Mon, process, Pid, Reason}, State) ->
    NewState = handle_down(Mon, Pid, Reason, State),
    {noreply, NewState};
handle_info(Unexpected, State) ->
    ok = io:format("~p Unexpected info: ~tp~n", [self(), Unexpected]),
    {noreply, State}.



%%% OTP Service Functions

-spec code_change(OldVersion, State, Extra) -> Result
    when OldVersion :: {down, Version} | Version,
         Version    :: term(),
         State      :: state(),
         Extra      :: term(),
         Result     :: {ok, NewState}
                     | {error, Reason :: term()},
         NewState   :: state().
%% @private
%% The gen_server:code_change/3 callback.
%% See: http://erlang.org/doc/man/gen_server.html#Module:code_change-3

code_change(_, State, _) ->
    {ok, State}.


-spec terminate(Reason, State) -> no_return()
    when Reason :: normal
                 | shutdown
                 | {shutdown, term()}
                 | term(),
         State  :: state().
%% @private
%% The gen_server:terminate/2 callback.
%% See: http://erlang.org/doc/man/gen_server.html#Module:terminate-2

terminate(_, _) ->
    ok.



%%% Doer Functions

-spec do_listen(PortNum, State) -> {Result, NewState}
    when PortNum  :: inet:port_number(),
         State    :: state(),
         Result   :: ok
                   | {error, Reason :: {listening, inet:port_number()}},
         NewState :: state().
%% @private
%% The "doer" procedure called when a "listen" message is received.

do_listen(PortNum, State = #s{port_num = none}) ->
    SocketOptions =
        [inet6,
         {packet,    2},
         {active,    once},
         {mode,      binary},
         {keepalive, true},
         {reuseaddr, true}],
    {ok, Listener} = gen_tcp:listen(PortNum, SocketOptions),
    {ok, _} = coon_client:start(Listener),
    {ok, State#s{port_num = PortNum, listener = Listener}};
do_listen(_, State = #s{port_num = PortNum}) ->
    ok = io:format("~p Already listening on ~p~n", [self(), PortNum]),
    {{error, {listening, PortNum}}, State}.


-spec do_ignore(State) -> NewState
    when State    :: state(),
         NewState :: state().
%% @private
%% The "doer" procedure called when an "ignore" message is received.

do_ignore(State = #s{listener = none}) ->
    State;
do_ignore(State = #s{listener = Listener}) ->
    ok = gen_tcp:close(Listener),
    State#s{port_num = none, listener = none}.


-spec do_enroll(PID, ID, PK, State) -> NewState
    when ID       :: binary(),
         PID      :: pid(),
         PK       :: binary(),
         State    :: state(),
         NewState :: state().

do_enroll(ID, PID, PK, State = #s{clients = Clients}) ->
    Node = #node{id = ID, pid = PID, pk = PK},
    case maps:find(PID, Clients) of
        error ->
            true = ets:insert(nodes, Node),
            Mon = monitor(process, PID),
            NewClients = maps:put(PID, {ID, Mon}, Clients),
            State#s{clients = NewClients};
        {ID, _} ->
            true = ets:insert(nodes, Node),
            State;
        {OldID, Mon} ->
            Message = "~w switching registration from ~w to ~w",
            ok = log(info, Message, [PID, OldID, ID]),
            true = ets:delete(nodes, OldID),
            true = ets:insert(nodes, Node),
            NewClients = maps:put(PID, {ID, Mon}, Clients),
            State#s{clients = NewClients}
    end.


-spec handle_down(Mon, Pid, Reason, State) -> NewState
    when Mon      :: reference(),
         Pid      :: pid(),
         Reason   :: term(),
         State    :: state(),
         NewState :: state().
%% @private
%% Deal with monitors. When a new process enrolls as a client a monitor is set and
%% the process is added to the client list. When the process terminates we receive
%% a 'DOWN' message from the monitor. More sophisticated work managers typically have
%% an "unenroll" function, but this echo service doesn't need one.

handle_down(Mon, PID, Reason, State = #s{clients = Clients}) ->
    case maps:take(PID, Clients) of
        {{ID, Mon}, NewClients} ->
            [Down] = ets:lookup(nodes, ID),
            true = ets:insert(nodes, Down#node{pid = none}),
            State#s{clients = NewClients};
        error ->
            Unexpected = {'DOWN', Mon, process, PID, Reason},
            ok = io:format("~p Unexpected info: ~tp~n", [self(), Unexpected]),
            State
    end.
